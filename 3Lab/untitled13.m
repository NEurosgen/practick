p=[1.38 4.92 -4.97 -9.07 1.65 -8.21 10.81 10.15 -3.32 -4.67 -5.45 9.34]
r=roots(p);
x=-10:0.01:10;
f1=polyval(p,x);
xd=r(imag(r)==0)

d1 =polyder(p);
y1=polyval(d1,x);
rd1=roots(d1);
rdd1=rd1(imag(rd1)==0)

d2 =polyder(d1);
y2=polyval(d2,x);
rd2=roots(d2);
rdd2=rd2(imag(rd2)==0)

d3 =polyder(d2);
y3=polyval(d3,x);
rd3=roots(d3);
rdd3=rd3(imag(rd3)==0)

figure
subplot(4,1,1);
title("Полином");
hold on
grid on
axis([-5 4 -10000 10000])
plot(x,f1);
plot(xd,0,"r*");

subplot(4,1,2);
title("Первая производная");
hold on
grid on
axis([-5 4 -10000 10000])
plot(x,y1);
plot(rdd1,0,"r*");

subplot(4,1,3);
title("Вторая производная");
hold on
grid on
axis([-5 4 -10000 10000])
plot(x,y2);
plot(rdd2,0,"r*");

subplot(4,1,4);
title("Третья производная");
hold on
grid on
axis([-5 4 -10000 10000])
plot(x,y3);
plot(rdd3,0,"r*");
