% конус
a=8.85;
b=8.40;
c=-1.18;
x0=-0.36;
y0=-1.52;
z0=0.29;
u=[-2:0.1:2]';
v=[0:0.05*pi:2*pi];
Z=a*u*cos(v)+z0;
X=b*u*sin(v)+x0;
Y=c*u*ones(size(v))+y0;
%гиперболойд однополосный 
a1=2.56;
b1=9.90;
c1=-3.50
x0=-1.17;
y0=0.66;
z0=1.89;

Z1=a1*cosh(u)*cos(v);
X1=b1*cosh(u)*sin(v);
Y1=c1*sinh(u);


s1=[45, 50]; %азимут и угол возвышения
s2=[10,30];
figure 


subplot(2,2,1)
surfl(X,Y,Z,s1)
view(40,50)

subplot(2,2,2)
surfl(X,Y,Z,s1)
view(30,100)


subplot(2,2,3)
surfl(X,Y,Z,s2)
view(40,50)


subplot(2,2,4)
surfl(X,Y,Z,s2)
view(30,100)
