p=[1.38 4.92 -4.97 -9.07 1.65 -8.21 10.81 10.15 -3.32 -4.67 -5.45 9.34]
pfun=@(x) 1.38*x.^11+4.92*x.^10-4.97*x.^9-9.07*x.^8+1.65*x.^7-8.21*x.^6+10.81*x.^5+10.15*x.^4-3.32*x.^3-4.67*x.^2-5.45*x.^1+9.34*x.^0;
pfun1=@(x)-pfun(x);
r=roots(p);
x=-10:0.01:10;
f1=polyval(p,x);
xd=r(imag(r)==0)

d1 =polyder(p);
y1=polyval(d1,x);
rd1=roots(d1);
rdd1=rd1(imag(rd1)==0)

d2 =polyder(d1);
y2=polyval(d2,x);
rd2=roots(d2);
rdd2=rd2(imag(rd2)==0)

d3 =polyder(d2);
y3=polyval(d3,x);
rd3=roots(d3);
rdd3=rd3(imag(rd3)==0)

extr=fminbnd(pfun,-4,-3);
extr(2)=fminbnd(pfun1,-1,0);
extr(3)=fminbnd(pfun,0,1);
extr(4)=fminbnd(pfun1,1,2);

for i=1:4
    fprintf("%d Delta=%d\n",i,abs(extr(i)-rdd1(i)));
end



