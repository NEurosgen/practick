p=[1.3 3.12 1.31 -0.4];
y_=1.7*x^3-7.26*x-2.13;
y=@(x) 1.7*x.^3-7.26*x-2.13;

k1=sort(roots(p));
k2=fzero(y,[-2,-1]);
k2(2)=fzero(y,[-0.5,0]);
k2(3)=fzero(y,[2,3]);

k3=solve(y_);
k3=vpa(k3,10);
k3=sort(k3);
k3=double(k3);

for i=1:3
    fprintf("%d Detail =%d\n",i,abs(k1(i)-k2(i)))
end

for i=1:3
    fprintf("%d Detail =%d\n",i,abs(k1(i)-k3(i)))
end
for i=1:3
    fprintf("%d Detail =%d\n",i,abs(k2(i)-k3(i)))
end




