function y=f(x)
    a=1.7;
    b=0.6;
    c=2.3;
    d=0.3;
     y=exp(a+b*x.^2).*sin(c+d*x.^2);
end 
