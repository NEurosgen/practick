syms x;
a=7;
t(x)=x+log(x)-0.5;

t1=taylor(t,x,a,'Order',2)
p1=sym2poly(t1);
t2=taylor(t,x,a,'Order',3)
p2=sym2poly(t2);
t3=taylor(t,x,a,'Order',4)
p3=sym2poly(t3);
t4=taylor(t,x,a,'Order',5)
p4=sym2poly(t4);

x1=a-7:0.001:a+7;

y1=polyval(p1,x1);
y2=polyval(p2,x1);
y3=polyval(p3,x1);
y4=polyval(p4,x1);

figure 
hold on
grid on

plot(x1,t(x1),'k','LineWidth',2);
plot(x1,y1,'g');
plot(x1,y2,'b');
plot(x1,y3,'r');
plot(x1,y4,'m');

plot(a,t(a),'rx')
legend('y(x)','2 слагаемых','3 слагаемых','4 слагаемых','5 слагаемых','Location','NorthWest');