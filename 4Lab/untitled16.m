syms x;
syms n;
%445
y1(x)=(sqrt(1-2*x-x^2)-(1+x))/x;
ly1=limit(y1(x),x,0);
ly1=simplify(ly1);
%540
y2(x)=log((n*x+sqrt(1-n^2*x^2))/(x+sqrt(1-x^2)));
ly2=limit(y2(x),x,0);
ly2=simplify(ly2);

%567
y3(x)=log(1+x*2.71^x)/(log(x+sqrt(1+x^2)));
ly3=limit(y3(x),x,0);
ly3=simplify(y3);

%870
g1(x)=(sin(x)-x*cos(x))/(cos(x)+x*sin(x));
dg1(x)=diff(g1);
dg1(x)=simplify(dg1);

%951
g2(x)=log(2.71^x+sqrt(1+2.71^(2*x)));
dg2(x)=diff(g2);
dg2(x)=simplify(dg2);

%1361
h0(x)=(2*atan(x)/3.14)^x;
h1(x)=log(2*atan(x)/3.14);
h2(x)=1/x;
dh1(x)=diff(h1);
dh2(x)=diff(h2);
lim=limit(dh1(x)/dh2(x),x,0);
lim=simplify(exp(lim));

fprintf("№ 445\n");
fprintf("Функция = \n");
pretty(y1);
fprintf("Предел= ");
pretty(ly1);


fprintf("№ 540\n");
fprintf("Функция = \n");
pretty(y2);
fprintf("Предел= ");
pretty(ly2);

fprintf("№ 567\n");
fprintf("Функция = \n");
pretty(y1);
fprintf("Предел= ");
pretty(ly2);


fprintf("№ 1351\n");
fprintf("Функция = \n");
pretty(h0);
fprintf("Предел= ");
pretty(lim);




fprintf("№ 870\n");
fprintf("Функция = \n");
pretty(g1);
fprintf("Производная=\n");
pretty(dg1);

fprintf("№ 981\n");
fprintf("Функция = \n");
pretty(g2);
fprintf("Производная=\n");
pretty(dg2);


