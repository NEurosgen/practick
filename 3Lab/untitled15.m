p=[1.38 4.92 -4.97 -9.07 1.65 -8.21 10.81 10.15 -3.32 -4.67 -5.45 9.34]
r=roots(p);
x=-10:0.01:10;
f1=polyval(p,x);
xd=r(imag(r)==0)

prand=[1.38 (4.92+4.92*(rand/10-0.05)) -4.97 -9.07 1.65 -8.21 10.81 10.15 -3.32 -4.67 -5.45 9.34]
r_rand=roots(prand);
prand=r_rand(imag(r_rand)==0)
for i=1:5
    fprintf("%i: %i\n",i,100*abs((r_rand(i)-xd(i))/xd(i)))
end
