function y=p(x)
    a=1.3;
    b=3.12;
    c=1.31;
    d=-0.4;
    y=a*x.^3+b*x.^2+c*x+d;
end