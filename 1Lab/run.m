x=[0:0.1:10];
y=f(x);
off =[1,-1];
figure;
hold on;
grid on;
plot(x,y);


[MinX,MinY]=fminbnd(@f,0,4);
plot(MinX,MinY,'>','color','r','LineWidth',1.5);
title(['e^(a+bx^2)sin(c+dx^2)'])
line([MinX,MinX+off(1)],[MinY MinY+off(2)],'color',[0 0 0]);
text(MinX+off(1),MinY+off(2),'Local minimum at [0;4]');

[MinX,MinY]=fminbnd(@f,4,8);
plot(MinX,MinY,'>','color','r','LineWidth',1.5);
line([MinX,MinX+off(1)],[MinY MinY+off(2)],'color',[0 0 0]);
text(MinX-off(1),MinY+off(2),'Local minimum at [4;8]');

[MinX,MinY]=fminbnd(@f,9,10);
plot(MinX,MinY,'>','color','r','LineWidth',1.5);
line([MinX,MinX+off(1)],[MinY MinY+off(2)],'color',[0 0 0]);
text(MinX-off(1),MinY-off(2),'Local minimum at [8;11]');
zx=fzero(@f,4);
plot(zx,0,'og');
line([zx zx+off(1)/2], [0 off(2)],'color',[0 0 0]);
text(zx+off(1)/2,off(2),'zero point')
zx=fzero(@f,8);
plot(zx,0,'og');
line([zx zx+off(1)/2], [0 off(2)],'color',[0 0 0]);
text(zx+off(1)/2,off(2),'zero point')
zx=fzero(@f,2);
plot(zx,0,'og');
line([zx zx+off(1)/2], [0 off(2)],'color',[0 0 0]);
text(zx+off(1)/2,off(2),'zero point');





